<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $stores app\models\Store */
?>

<div class="container">
    <h2 class="text-center">Import Product</h2>
    <div class="row">
        <div class="import-product-wrap">
            <form method="post" enctype="multipart/form-data">
                <div class="card border-primary rounded-0">
                    <div class="card-body p-3">

                        <!--Body-->
                        <div class="form-group">
                            <input type="file" class="form-control" id="upload_file" name="upload_file[]" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" multiple required />
                        </div>
                        <div class="form-group">
                            <select class="form-control" data-style="btn-primary" name="store_id">
                                <?php foreach ($stores as $store): ?>
                                    <option value="<?= $store->id ?>"><?= $store->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Import" class="btn btn-info btn-block rounded-0 py-2">
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<?php
$this->registerJs("
    $(document).on('submit', 'form', function (e) {
        e.preventDefault();
        var data = new FormData(this);
        console.log(data);
         $.ajax({
             type: 'POST',
             url: '/store/json-import-product',
             data: data,
             dataType: 'json',
             contentType : false,
             processData: false,
             success: function (data) {
                console.log(data);
             },
             error: function (xhr, status, error) {
                 console.log(xhr.responseText);
             }
         })
    })
");