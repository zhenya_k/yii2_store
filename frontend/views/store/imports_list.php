<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-index">

    <p>
        <?= Html::a('Import Product', ['import'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'import_number',
                'value' => function ($data) {
                    return $data->id;
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    $id = $data->queue_number;
                    if (Yii::$app->queue->isWaiting($id)) {
                        return \common\models\Import::STATUS_NEW;
                    } elseif (Yii::$app->queue->isReserved($id)) {
                        return \common\models\Import::STATUS_PROCESSING;
                    } elseif (Yii::$app->queue->isDone($id)) {
                        return \common\models\Import::STATUS_DONE;
                    }
                }
            ],
            [
                'attribute' => 'store',
                'value' => function ($data) {
                    return $data->store->title;
                }
            ],
            [
                'attribute' => 'products_upc',
                'value' => function ($data) {
                    return implode(", ", array_column($data->storeProducts, 'upc'));
                }
            ],
            [
                'attribute' => 'Success',
                'value' => function ($data) {
                    if ($data->success) {
                        return 'Pass';
                    } else {
                        return 'Fail';
                    }
                }
            ],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
