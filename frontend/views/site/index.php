<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>

        <p><a class="btn btn-lg btn-success" href="/site/imports-list">Imports</a></p>
    </div>
</div>
