<?php
namespace common\jobs;

use common\models\Import;
use common\models\StoreProduct;

class ImportProduct extends \yii\base\BaseObject implements \yii\queue\JobInterface
{
    public $file;
    public $store_id;
    public $import_id;
    
    public function execute($queue)
    {
        if (empty($this->store_id)) {
            throw new \Exception('Store Id is missing parameter');
        }
        if (empty($this->import_id)) {
            throw new \Exception('Import Id is missing parameter');
        }

        $csvData = [];
        $indext = 0;
        $fp = fopen($this->file, 'r');
        $lineKeys = fgetcsv($fp, 1000, ",");
        $line = fgetcsv($fp, 1000, ",");
        while ($line != false) {
            foreach ($lineKeys as $index => $key) {
                $csvData[$indext][$key] = $line[$index];
            }
            $indext++;
            $line = fgetcsv($fp, 1000, ",");
        }
        $import = Import::findOne($this->import_id);
        $updateImport = true;
        foreach ($csvData as $product) {
            if (!empty($product['upc']) && !empty($product['title']) && !empty($product['price'])) {
                $storeProduct = StoreProduct::findOne(['upc' => $product['upc'], 'store_id' => $this->store_id]);
                if (empty($storeProduct)) {
                    $storeProduct = new StoreProduct();
                    $storeProduct->upc = $product['upc'];
                    $storeProduct->store_id = $this->store_id;
                }
                $storeProduct->title = $product['title'];
                $storeProduct->price = $product['price'];
                $storeProduct->import_id = $this->import_id;
                $storeProduct->save();
            }
            if (empty($product['upc']) && $updateImport) {
                $updateImport = false;
                $import->success = 0;
                $import->save();
            }
        }
    }
}