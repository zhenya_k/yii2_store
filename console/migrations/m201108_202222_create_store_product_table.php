<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%store_product}}`.
 */
class m201108_202222_create_store_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%store_product}}', [
            'id' => $this->primaryKey(),
            'store_id' => $this->integer()->notNull(),
            'import_id' => $this->integer()->notNull(),
            'upc' => $this->string()->notNull(),
            'title' => $this->string(255),
            'price' => $this->money(9, 2)
        ]);

        $this->createIndex(
            'idx-store_product-store_id',
            '{{%store_product}}',
            'store_id'
        );

        $this->addForeignKey(
            'fk-store_product-store_id',
            '{{%store_product}}',
            'store_id',
            '{{%store}}',
            'id',
            'cascade'
        );

        $this->createIndex(
            'idx-store_product-import_id',
            '{{%store_product}}',
            'import_id'
        );

        $this->addForeignKey(
            'fk-store_product-import_id',
            '{{%store_product}}',
            'import_id',
            '{{%import}}',
            'id',
            'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-store_product-store_id',
            '{{%store_product}}'
        );
        $this->dropIndex(
            'idx-store_product-store_id',
            '{{%store_product}}'
        );
        $this->dropForeignKey(
            'fk-store_product-import_id',
            '{{%store_product}}'
        );
        $this->dropIndex(
            'idx-store_product-import_id',
            '{{%store_product}}'
        );
        $this->dropTable('{{%store_product}}');
    }
}
