<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%import}}`.
 */
class m201108_200714_create_import_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%import}}', [
            'id' => $this->primaryKey(),
            'store_id' => $this->integer()->notNull(),
            'queue_number' => $this->integer(),
            'success' => $this->smallInteger(1)->notNull()->defaultValue(1)
        ]);

        $this->createIndex(
            'idx-import-store_id',
            '{{%import}}',
            'store_id'
        );

        $this->addForeignKey(
            'fk-import-store_id',
            '{{%import}}',
            'store_id',
            '{{%store}}',
            'id',
            'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-import-store_id',
            '{{%import}}'
        );
        $this->dropIndex(
            'idx-import-store_id',
            '{{%import}}'
        );
        $this->dropTable('{{%import}}');
    }
}
